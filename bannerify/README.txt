Dont look in the "old" folder, this is what the code looked like before it was made into a jQuery object. 
Just there for reference. 

/*********************************/
/*                               */
/*          Dependancies:        */
/*                               */
/*********************************/

- You'll need to have jquery          
  >>  https://jquery.com

- You'll need to have slick.js        
  >> http://kenwheeler.github.io/slick/
  this is the slider we're using. Technically you should have access to all their 
  options, methods, and events. Joyous.

- You'll need YoutubeIframeApi        
  >> https://developers.google.com/youtube/iframe_api_reference
  This is what allows us to get playback state, and events so we can alter the slider 
  behavior to account for it.

- You'll need to have jquery debouce  
  >> http://benalman.com/code/projects/jquery-throttle-debounce/jquery.ba-throttle-debounce.js
  This is technically not neccessary, but highly recommended. When resizing a window an event 
  fires super quick hundreds of times. This will debounce those requests.

/*********************************/
/*                               */
/*          Usage:               */
/*                               */
/*********************************/
> Include the js and css files.

- look in the examples folder. but essentially it's a jquery plugin that you'll chain to 
  "slick.js" plugin.

  << Basic Initialization >>

  ```
    $('.example').slick({
      touchMove     : false,
      autoplaySpeed : 5000,
      dots          : true,
      arrows        : false
    }).bannerify({
      dotColor: "#ccc",
      dotHoverColor: "red",
      dotPositionClass: "top right"
    });
  ```

  << Youtube Initialization >>

  You need to add this. Further versions might replace the need to write this. You will 
  add another $('YOURSELECTOR').youtubeReady() for each additional slider you have.
  ```
    function onYouTubeIframeAPIReady() {
      $('.example').youtubeReady();
    }

  ```
  If you choose to put all this code in an IIFE. you'll have to make the above function available 
  to the window object.
  ```
    window.onYouTubeIframeAPIReady = onYouTubeIframeAPIReady;
  ```

  << Video Resizing >>

  To get video resizing working properly, you'll need this. You'll need to do this for each bannerify object you have.
  ```
  $(window).resize( $.debounce( 250, false, function(evt){ 
    $('example').adjustVideoHeight();
  }));
  ```

/*********************************/
/*                               */
/*          Features:            */
/*                               */
/*********************************/

- fully responsive (adapts to 100% the width of its container)
  - scales videos nicely
  - finger swipe navigation.
  - can choose to remove dots

- pauses the video when it's started, and automatically restarts 
  if videos ends, or a is paused for x amount of time

- pauses the video when next slide button is hit.

- height is determined by largest image, videos will automatically scale to fit this.

- easy to implement (simple jQuery Plugin), easy to extend.

- fully customizable
  - colors
  - delays
  - navigation position.

- you can have multiple on the same page.

- can contain captions