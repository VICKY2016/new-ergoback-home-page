/**
  * Caveats: each videos div should have unique ID! so no id=video1 twice. 
  */

(function($) {
  'use strict';

  /**
    * source: https://css-tricks.com/snippets/javascript/inject-new-css-rules/
    */
  function injectStyles(rule) {
    var div = $("<div />", {
      html: '&shy;<style>' + rule + '</style>'
    }).appendTo("body");    
  }

  /**
    * This is where we deal with all the video states
    */
  var YoutubePlayerManager = function(params) {
    this.players = {};

    /**
      * 
      */
    this.getCount = function() {};

    /**
      * Returns true if a slider contains a gived videoID
      */
    this.contains = function(videoID) {
      for (var heldVideoID in this.players) {
        if (heldVideoID === videoID) {
          return true
        }
      }
      return false 
    };

    /**
      * adds a video
      */
    this.add = function(params) {
      //if some of the params are missing dont add the player.
      if (!params.selectorID) {return}

      var player = new YT.Player(params.selectorID, {
        videoId: params.videoID,
        playerVars: {
          rel: 0,
          showinfo: 0 
        },
        events: {
          'onStateChange': params.stateChangeEventCb
        }
      });

      this.players[params.videoID] = player;
    }; //end add

    /**
      * returns true if a video is playing
      */
    this.isPlaying = function() {
      for (var videoID in this.players) {
        if (this.players.hasOwnProperty(videoID)) {
          if (this.players[videoID].getPlayerState() === 1) {
            return true
          }
        }
      }
      return false
    }; //end isPlaying

    /**
      * Pauses all the videos in the object
      */
    this.pause = function() {
      for (var videoID in this.players) {
        if (this.players.hasOwnProperty(videoID)) {
          if (this.players[videoID].getPlayerState && this.players[videoID].getPlayerState() === 1) {
            this.players[videoID].pauseVideo();
          }
        }
      }
    }; //end pause
  };


  /**
    *
    */
  $.fn.bannerify = function( options ) {

    var videos            = new YoutubePlayerManager(),
        self              = this;

    var settings = $.extend({
      dotPositionClass  : "bottom left",
      bannerPauseDelay  : 10000,
      dotColor          : 'white',
      dotHoverColor     : 'green'
    }, options );

    this.find('.slick-dots').addClass(settings.dotPositionClass);
    injectStyles(this.selector + ' .slick-dots li button { background: ' + settings.dotColor + '; }');
    injectStyles(this.selector + ' .slick-dots li.slick-active button { background: ' + settings.dotHoverColor + '; }');

    this.on('beforeChange', function() {
      videos.pause();
      self.slick('slickPlay');
    });

    this.slick('slickPlay');

    this.contains = function(videoID) {
      return videos.contains(videoID)
    }

    /**
      * the banner is dependant on the image heights. Not the video heights.
      * So in order to resize the videos we need to set all their heights to 0
      * determine the height left by the slider, than resize it to the height of the largest image.
      * NOTE: If you could do this via pure css, bless your soul.
      */
    this.adjustVideoHeight = function() {
      var iframes = this.find('iframe');
      iframes.height(0);
      var bannerHeight = this.height();
      iframes.height(bannerHeight);
    };

    /**
      * This is called from the onYouTubeIframeAPIReady() event fired by the 
      * Youtube Iframe API. We need this API to keep track of the events fired by the player.
      * Without it, we wouldn't have the fancy conditionals we oh so love (eg. pause slick on play)
      */
    this.youtubeReady = function() {
      $.each(this.find('.youtube-video'), function(index, value) {
        var videoID     = $(value).attr("data-video-id");
        var selectorID  = $(value).attr("id");


        //console.log("trying to add: ", videoID)
        videos.add({
          selectorID  : selectorID,
          videoID     : videoID,
          stateChangeEventCb : self.stateChangeEvent
        });

        self.adjustVideoHeight();
      });
    }

    /**
      * Handle events for the youtube player.
      */
    this.stateChangeEvent = function(evt) {
      var videoID = evt.target.getVideoData()['video_id'];
      if (!self.contains(videoID)) {
        return
      }
      //playing
      if(evt.data === 1 || evt.data === 3) {
        self.slick('slickPause');
      } 
      //ended
      else if(evt.data === 0) {
        self.slick('slickPlay');
      }
      //paused
      else if(evt.data === 2) {
        setTimeout(function(){
          if (!videos.isPlaying()) {
            self.slick('slickPlay');
          }
        }, settings.bannerPauseDelay);
      }
    }
    
    //return this.
  };
} )(jQuery);